# Create EKS Cluster & Node Groups

## Step-00: Introduction
- Understand about EKS Core Objects
  - Control Plane
  - Worker Nodes & Node Groups
  - VPC
- Create EKS Cluster
- Associate EKS Cluster to IAM OIDC Provider
- Create EKS Node Groups
- Verify Cluster, Node Groups, EC2 Instances, IAM Policies and Node Groups
- Create a new EC2 Keypair with name as `kube-demo`
- This keypair we will use it when creating the EKS NodeGroup.
- This will help us to login to the EKS Worker Nodes using Terminal.

## Step-01: Create EKS Cluster using eksctl

Note: 

- VPC should have tags (Ref: https://aws.amazon.com/premiumsupport/knowledge-center/eks-vpc-subnet-discovery/)
- Modify the IP, VPC ID and subnet as required.
- Create NAT and add a new route table.
- Change Public key Path.

It will take 15 to 20 minutes to create the Cluster Control Plane 

Create the **cluster** file with below contents.

```
apiVersion: eksctl.io/v1alpha5
cloudWatch:
  clusterLogging: {}
iam:
  vpcResourceControllerPolicy: true
  withOIDC: true
kind: ClusterConfig
managedNodeGroups:
- amiFamily: AmazonLinux2
  desiredCapacity: 2
  disableIMDSv1: false
  disablePodIMDS: false
  iam:
    withAddonPolicies:
      albIngress: true
      appMesh: true
      appMeshPreview: false
      autoScaler: true
      certManager: false
      cloudWatch: false
      ebs: false
      efs: false
      externalDNS: true
      fsx: false
      imageBuilder: true
      xRay: false
  instanceSelector: {}
  instanceType: t3a.large
  labels:
    alpha.eksctl.io/cluster-name: XDP-K8S-POC
    alpha.eksctl.io/nodegroup-name: ng-439e4ea1
  maxSize: 4
  minSize: 2
  name: ng-439e4ea1
  privateNetworking: true
  releaseVersion: ""
  securityGroups:
    withLocal: null
    withShared: null
  ssh:
    allow: true
    enableSsm: false
    publicKeyPath: XDP-K8S-POC
  tags:
    alpha.eksctl.io/nodegroup-name: ng-439e4ea1
    alpha.eksctl.io/nodegroup-type: managed
  volumeSize: 20
  volumeType: gp3
metadata:
  name: XDP-K8S-POC
  region: ca-central-1
  version: "1.19"
privateCluster:
  enabled: false
vpc:
  autoAllocateIPv6: false
  cidr: 172.31.0.0/16
  clusterEndpoints:
    privateAccess: false
    publicAccess: true
  id: vpc-0593b46c
  manageSharedNodeSecurityGroupRules: true
  nat:
    gateway: Single
  subnets:
    private:
      ca-central-1a:
        az: ca-central-1a
        cidr: 172.31.48.0/20
        id: subnet-0119fe736b57ca0ef
      ca-central-1b:
        az: ca-central-1b
        cidr: 172.31.64.0/20
        id: subnet-02c8de9cb43612c98
    public:
      ca-central-1a:
        az: ca-central-1a
        cidr: 172.31.16.0/20
        id: subnet-3d8eb854
      ca-central-1b:
        az: ca-central-1b
        cidr: 172.31.0.0/20
        id: subnet-6766281c

```

```
# Create Cluster
eksctl create cluster -f cluster

# Get List of clusters
eksctl get cluster                  
```
## Step-02: Verify Cluster & Nodes

### Verify NodeGroup subnets to confirm EC2 Instances are in Public Subnet
- Verify the node group subnet to ensure it created in public subnets
  - Go to Services -> EKS -> eksdemo -> eksdemo1-ng1-public
  - Click on Associated subnet in **Details** tab
  - Click on **Route Table** Tab.
  - We should see that internet route via Internet Gateway (0.0.0.0/0 -> igw-xxxxxxxx)

### Verify Cluster, NodeGroup in EKS Management Console
- Go to Services -> Elastic Kubernetes Service -> eksdemo1

### List Worker Nodes
```
# List EKS clusters
eksctl get cluster

# List NodeGroups in a cluster
eksctl get nodegroup --cluster=<clusterName>

# List Nodes in current kubernetes cluster
kubectl get nodes -o wide

# Our kubectl context should be automatically changed to new cluster
kubectl config view --minify
```

### Verify Worker Node IAM Role and list of Policies
- Go to Services -> EC2 -> Worker Nodes
- Click on **IAM Role associated to EC2 Worker Nodes**

### Verify Security Group Associated to Worker Nodes
- Go to Services -> EC2 -> Worker Nodes
- Click on **Security Group** associated to EC2 Instance which contains `remote` in the name.

### Verify CloudFormation Stacks
- Verify Control Plane Stack & Events
- Verify NodeGroup Stack & Events

### Login to Worker Node using Keypai kube-demo
- Login to worker node
```
# For MAC or Linux or Windows10
ssh -i kube-demo.pem ec2-user@<Public-IP-of-Worker-Node>

# For Windows 7
Use putty
```

## Step-06: Update Worker Nodes Security Group to allow all traffic
- We need to allow `All Traffic` on worker node security group

## Additional References
- https://docs.aws.amazon.com/eks/latest/userguide/enable-iam-roles-for-service-accounts.html
- https://docs.aws.amazon.com/eks/latest/userguide/create-service-account-iam-policy-and-role.html